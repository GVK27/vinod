﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using NSABP_Model_BO_;
namespace NSABP_DAL
{
    public class Configuration_DAL
    {
        private static string Co = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
        private static SqlConnection Conn;
        private static StringBuilder sb_qry = new StringBuilder();
        //public static Dictionary<int, string> GetForms()
        //{

        //    Dictionary<int, string> list_forms = new Dictionary<int, string>();
        //    try
        //    {
        //        Conn = new SqlConnection(Co);

        //        SqlCommand cmd = new SqlCommand("select Form_ID, Form_Name from T7_Forms where Record_Status='Active'", Conn);
        //        Conn.Open();
        //        SqlDataReader dr = cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            list_forms.Add(Convert.ToInt16(dr["Form_ID"]), dr["Form_Name"].ToString());

        //        }
        //        Conn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    { }
        //    return list_forms;
        //}
        public static List<checkboxes_BO> GetForms()
        {


            List<checkboxes_BO> list_result = new List<checkboxes_BO>();
            try
            {
                Conn = new SqlConnection(Co);

                SqlCommand cmd = new SqlCommand("select Form_ID, Form_Name from T7_Forms where Record_Status='Active'", Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    checkboxes_BO bo = new checkboxes_BO();
                    bo.ID = Convert.ToInt16(dr["Form_ID"]);
                    bo.Name = dr["Form_Name"].ToString();
                    bo._isChecked = false;
                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();

            }
            return list_result;
        }
        public static List<int> GetForms_IDS(int protocol_ID)
        {
            List<int> list_result = new List<int>();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("select distinct Form_ID from T7_Forms t7  join T100_Map_T2Protocol_T7Forms t100 on t100.T7_FormID = t7.Form_ID where t7.Record_Status = 'Active' and t100.T2_ProtocolID = " + protocol_ID + "", Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    list_result.Add(Convert.ToInt16(dr["Form_ID"]));
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();

            }
            return list_result;
        }
        public static bool Config_Protocol_form(int Protocol_ID, string[] Form_ID)
        {
            bool isSave = false;
            try
            {

                Conn = new SqlConnection(Co);
                sb_qry.Append("delete from T100_Map_T2Protocol_T7Forms where T2_ProtocolID=" + Protocol_ID + "  ");
                foreach (string item in Form_ID)
                {

                    sb_qry.Append("INSERT INTO[dbo].[T100_Map_T2Protocol_T7Forms]([T2_ProtocolID],[T7_FormID],[Record_Status])VALUES(" + Protocol_ID + ", '" + item + "', 'Active')");
                }
                SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                cmd.ExecuteNonQuery();
                Conn.Close();
                isSave = true;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Conn.Close();
            }
            return isSave;
        }
        //changes start For Timepoint10/10
        public static bool Config_Protocol_form_Timepoint(List<checkboxes_BO> listbo)
        {
            bool isSave = false;
            try
            {
                List<int> mapid = new List<int>();
                Conn = new SqlConnection(Co); int i;
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = Conn;
                Conn.Open();
                //sb_qry.Append("select [Map_ID] from T100_Map_T2Protocol_T7Forms where T2_ProtocolID=" + listbo.Select(c => c.Protocol_ID).FirstOrDefault() + "  ");
                //cmd.CommandText = sb_qry.ToString();
                //SqlDataReader dr= cmd.ExecuteReader();
                //sb_qry.Clear();
                //while(dr.Read())
                //{
                //    mapid.Add(Convert.ToInt32(dr["Map_ID"]));
                //}

                sb_qry.Append("delete from T100_Map_T2Protocol_T7Forms where T2_ProtocolID=" + listbo.Select(c => c.Protocol_ID).FirstOrDefault() + "; delete from T102_Map_ProtocolForm_Timepints where T2_ProtocolID=" + listbo.Select(c => c.Protocol_ID).FirstOrDefault() + " ");

                foreach (checkboxes_BO item in listbo)
                {

                    sb_qry.Append("INSERT INTO[dbo].[T100_Map_T2Protocol_T7Forms]([T2_ProtocolID],[T7_FormID],[Record_Status])VALUES(" + item.Protocol_ID + ", '" + item.ID + "', 'Active');select SCOPE_IDENTITY()");
                    cmd.CommandText = sb_qry.ToString();
                    i = Convert.ToInt16(cmd.ExecuteScalar());
                    sb_qry.Clear();
                    if (item.StrArray != null)
                    {
                        foreach (var li in item.StrArray)
                        {
                            sb_qry.Append(" insert into T102_Map_ProtocolForm_Timepints(T100_MapId,TimePoint,T2_ProtocolID)values(" + i + ",'" + li + "'," + item.Protocol_ID + ")");
                        }
                        cmd.CommandText = sb_qry.ToString();
                        cmd.ExecuteNonQuery();
                        sb_qry.Clear();
                    }
                }
                Conn.Close();
                isSave = true;
            }
            catch (Exception ex)
            {
            }
            return isSave;
        }
        public static DataTable GetForms_TimePoints(int protocol_ID)
        {
            //List<int> list_result = new List<int>();
            //List<checkboxes_BO> bolist = new List<checkboxes_BO>();
            //checkboxes_BO bo;
            DataTable dt = new DataTable();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("select distinct t102.Id, T7_FormID,TimePoint from T100_Map_T2Protocol_T7Forms t100 left join [T102_Map_ProtocolForm_Timepints] t102 on t100.Map_ID=t102.T100_MapId   where t100.Record_Status = 'Active' and t100.T2_ProtocolID = " + protocol_ID + "", Conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
            }

            return dt;
        }
        //changes end For Timepoint10/10


    }
}
