﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using NSABP_Model_BO_;
namespace NSABP_DAL
{
    public class User_DAL
    {
        private static string Co = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
        private static SqlConnection Conn;
        private static StringBuilder sb_qry;
        //public static DataTable UserStatus()
        //{
        //    DataTable dt = new DataTable();
        //    try
        //    {
        //        //string co = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
        //        //SqlConnection conn = new SqlConnection(co);
        //        conn = new SqlConnection(co);
        //        SqlCommand cmd = new SqlCommand("select Status_ID,Status_Title from T53_User_Status where Record_Status='Active'", conn);
        //        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //        da.Fill(dt);

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return dt;
        //}
        //public static DataTable Role()
        //{
        //    DataTable dt = new DataTable();
        //    try
        //    {
        //        string co = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;

        //        SqlConnection conn = new SqlConnection(co);
        //        SqlCommand cmd = new SqlCommand("select  ID,Role_Name from T5_User_Roles where Record_Status='Active'", conn);
        //        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //        da.Fill(dt);

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return dt;

        //}
        //public IEnumerable<User_BO> user
        //{
        //    get
        //    {
        //        string co = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
        //        List<User_BO> list_user_bo = new List<User_BO>();
        //        using (SqlConnection con = new SqlConnection(co))
        //        {
        //            SqlCommand cmd = new SqlCommand("select * from T1_User_Main", con);
        //            con.Open();
        //            SqlDataReader dr = cmd.ExecuteReader();
        //            while (dr.Read())
        //            {
        //                User_BO bo = new User_BO();
        //                bo.FName = dr["FName"].ToString();
        //                bo.LName = dr["LName"].ToString();

        //                bo.email = dr["email"].ToString();
        //                bo.UserName = dr["UserName"].ToString();
        //                bo.User_ID = Convert.ToInt16(dr["User_ID"]);
        //                bo.Record_Status = dr["Record_Status"].ToString();
        //                bo.Created_on = Convert.ToDateTime(dr["Created_on"]);
        //                list_user_bo.Add(bo);
        //            }
        //            return list_user_bo;
        //        }
        //    }
        //}
        public static DataTable GetProtocols()
        {
            DataTable dt = new DataTable();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("select Id,Protocol_Code from T2_Protocol_Main where Record_Status='Active'", Conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
            }
            return dt;

        }
        public static List<User_BO> GetAllUsers()
        {
            List<User_BO> list_user_bo = new List<User_BO>();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("Sp_GetUserDetails", Conn);
                cmd.CommandType = CommandType.StoredProcedure;
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    User_BO bo = new User_BO();

                    bo.FName = dr["FName"].ToString();
                    bo.LName = dr["LName"].ToString();
                    bo.email = dr["email"].ToString();
                    bo.Work_Title = dr["Work_Title"].ToString();
                    if (dr["PhoneNumber"] != DBNull.Value)
                        bo.PhoneNo = dr["PhoneNumber"].ToString();
                    bo.MInitial = dr["MInitial"].ToString();
                    bo.UserName = dr["UserName"].ToString();
                    bo.User_ID = Convert.ToInt16(dr["User_ID"]);
                    bo.Record_Status = dr["Record_Status"].ToString();
                    bo.Created_on = Convert.ToDateTime(dr["Created_on"]);
                    //if (dr["Protocol_Id"] != DBNull.Value)
                    //    bo.Protocol_Id = Convert.ToInt32(dr["Protocol_Id"]);
                    //bo.Protocol_Code = dr["Protocol_Code"].ToString();
                    if (dr["T6_User_Status"] != DBNull.Value)
                        bo.Status_Id = Convert.ToInt32(dr["T6_User_Status"]);
                    bo.Status_Title = dr["Status_Title"].ToString();
                    //if (dr["Role_Id"] != DBNull.Value)
                    //    bo.Role_Id = Convert.ToInt32(dr["Role_Id"]);
                    //bo.Role_Name = dr["Role_Name"].ToString();


                    //if (dr["site"] != DBNull.Value)
                    //    bo.Site = dr["site"].ToString();
                    bo.List_Protocol_role_site = ListProtocol_role_site(bo.User_ID);
                    list_user_bo.Add(bo);

                }
                Conn.Close();

            }
            catch (Exception ex)
            {
            }

            return list_user_bo;
        }
        public static bool SaveUserDetails(User_BO bo)
        {
            bool isSave = false;
            try
            {

                Conn = new SqlConnection(Co);
                string strQuery = "insert into T1_User_Main (UserName, Password, Work_Title, FName, MInitial, LName, email, T6_User_Status, Created_by, Created_on,PhoneNumber,Record_Status) values('" + bo.UserName + "','" + bo.Password + "','" + bo.Work_Title + "','" + bo.FName + "','" + bo.MInitial + "','" + bo.LName + "','" + bo.email + "'," + bo.Status_Id + "," + bo.Created_by + ",getdate(),'" + bo.PhoneNo + "','Active') select SCOPE_IDENTITY()";
                //if (!String.IsNullOrEmpty(bo.Site) || bo.Site != "")
                //{
                //    strQuery += ",'" + bo.Site + "')";
                //}
                //else
                //{
                //    strQuery += ",null)";
                //}
                //SqlCommand cmd = new SqlCommand("insert into T1_User_Main (UserName, Password, Work_Title, FName, MInitial, LName, email, Protocol_Id, Role_Id,T6_User_Status, Created_by, Created_on,PhoneNumber, Record_Status) values('" + bo.UserName + "','" + bo.Password + "','" + bo.Work_Title + "','" + bo.FName + "','" + bo.MInitial + "','" + bo.LName + "','" + bo.email + "'," + bo.Protocol_Id + "," + bo.Role_Id + "," + bo.Status_Id + "," + bo.Created_by + ",getdate(),'"+bo.PhoneNo+"','Active')", Conn);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = strQuery;
                cmd.Connection = Conn;
                Conn.Open();
                int id = Convert.ToInt16(cmd.ExecuteScalar());
                foreach (var item in bo.List_Protocol_role_site)
                {
                    sb_qry.Append(" INSERT INTO[T101_Map_Protocol_User_Role]([T1_User_ID],[T2_Protocol_Id],[T5_Role_ID],[T8_sites_Id])VALUES (" + id + "," + item.ProtocolID + "," + item.T5_Role_ID + ",");
                    if (item.T8_sites_Id != "")
                        sb_qry.Append(" '" + item.T8_sites_Id + "')");
                    else
                        sb_qry.Append("null)");

                    cmd.CommandText = sb_qry.ToString();
                    cmd.ExecuteNonQuery();
                    sb_qry.Clear();
                }


                Conn.Close();
                isSave = true;
            }
            catch (Exception ex)
            {

            }
            return isSave;
        }
        public static DataSet GetProtocols_Roles_Status()
        {
            DataSet ds = new DataSet();
            try
            {
                //    SqlConnection conn = new SqlConnection(co);
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("Sp_GetProtocol_userRoles_Status", Conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception)
            {
            }
            return ds;
        }
        public static bool UpdateUserDetails(User_BO bo)
        {
            bool isUpdate = false;
            try
            {

                Conn = new SqlConnection(Co);
                string StrQuery = "Update T1_User_Main set  Work_Title='" + bo.Work_Title + "', FName='" + bo.FName + "', MInitial='" + bo.MInitial + "', LName='" + bo.LName + "', email='" + bo.email + "',T6_User_Status=" + bo.Status_Id + ",PhoneNumber='" + bo.PhoneNo + "' ";
                StrQuery += " where User_ID = " + bo.User_ID + "";

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = StrQuery;
                cmd.Connection = Conn;
                Conn.Open();
                cmd.ExecuteNonQuery();

                cmd.CommandText = "delete from[T101_Map_Protocol_User_Role] where T1_User_ID = " + bo.User_ID +"";
                cmd.ExecuteNonQuery();

                sb_qry = new StringBuilder();
                foreach (var item in bo.List_Protocol_role_site)
                {
                    sb_qry.Append(" INSERT INTO[dbo].[T101_Map_Protocol_User_Role] ([T1_User_ID],[T2_Protocol_Id],[T5_Role_ID],[T8_sites_Id]) ");
                    sb_qry.Append("  VALUES (" + bo.User_ID + "," + item.ProtocolID + "," + item.T5_Role_ID + ",'" + item.T8_sites_Id + "' ) ");
                    cmd.CommandText = sb_qry.ToString();
                    cmd.ExecuteNonQuery();
                    sb_qry.Clear();
                }



                Conn.Close();
                isUpdate = true;
            }
            catch (Exception ex)
            {
            }
            return isUpdate;
        }
        public static bool IsUserExists(string username)
        {
            bool isExists = false;
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                sb_qry.Append("select count(*) from T1_User_Main where USERNAME = '" + username.Trim() + "' and Record_Status = 'Active'");

                SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                Conn.Close();
                if (count != 0)
                    isExists = true;
            }
            catch (Exception ex)
            {
            }
            return isExists;
        }


        public static List<Protocol_role_site> ListProtocol_role_site(int user_ID)
        {
            List<Protocol_role_site> ListProtocol_role_site = new List<Protocol_role_site>();
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                SqlCommand cmd = new SqlCommand();
                sb_qry.Append(" SELECT T101.[id],[T1_User_ID],T2.[Id] as ProtocolID,T2.[Protocol_Code],[T8_sites_Id],[T5_Role_ID],T5.Role_Name ");
                sb_qry.Append(" FROM T101_Map_Protocol_User_Role T101 join T1_User_Main T1 on(T1.User_ID= t101.[T1_User_ID]) ");
                sb_qry.Append(" Left join T2_Protocol_Main T2 on(T2.Id = T101.[T2_Protocol_Id]) ");
                sb_qry.Append(" join[dbo].[T5_User_Roles] T5 on(T5.ID = T101.[T5_Role_ID])");


                sb_qry.Append(" where t1.Record_Status='Active' and T1.t6_User_Status=1 and  T1.User_ID=" + user_ID + "");
                cmd.CommandText = sb_qry.ToString();
                cmd.Connection = Conn;
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Protocol_role_site bo = new Protocol_role_site();

                    bo.T1_User_ID = Convert.ToInt16(dr["T1_User_ID"]);
                    if(dr["ProtocolID"]!=DBNull.Value)
                    bo.ProtocolID = Convert.ToInt16(dr["ProtocolID"]);
                    bo.T5_Role_ID = Convert.ToInt16(dr["T5_Role_ID"]);

                    bo.Protocol_Code = dr["Protocol_Code"].ToString();
                    bo.Role_Name = dr["Role_Name"].ToString();
                    if (dr["T8_sites_Id"] != DBNull.Value)
                        bo.T8_sites_Id = dr["T8_sites_Id"].ToString();
                    ListProtocol_role_site.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            return ListProtocol_role_site;

        }

        public static bool Changepwd( string pwd,int UserId)
        {
            bool issave = false;
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                sb_qry.Append("update T1_User_Main set Password='"+ pwd + "' where  User_ID="+ UserId + "");

                SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                int count = Convert.ToInt32(cmd.ExecuteNonQuery());
                Conn.Close();
                if (count > 0)
                    issave = true;

            }
            catch (Exception ex)
            {

               
            }
            return issave;

        }
    }
}
