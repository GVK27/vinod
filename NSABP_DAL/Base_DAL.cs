﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using NSABP_Model_BO_;

namespace NSABP_DAL
{
    public class Base_DAL
    {
        private static string Co = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
        private static SqlConnection Conn;
        private static StringBuilder sb_qry = new StringBuilder();
        public static List<Sites> list_Sites()
        {
            List<Sites> list_result = new List<Sites>();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("SELECT [ID],[SiteID],[Site_Name] FROM [dbo].[T8_Sites] where Record_Status='Active'", Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Sites bo = new Sites();
                    bo.ID = Convert.ToInt16(dr["ID"]);
                    bo.SiteID = dr["SiteID"].ToString();
                    bo.Site_Name = dr["Site_Name"].ToString();
                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return list_result;
        }
        public static List<Shipping_Company> list_Shipping_Company()
        {
            List<Shipping_Company> list_result = new List<Shipping_Company>();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("SELECT  [ID],[Company_name] FROM [T50_Shipping_Company] where Record_Status='Active'", Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Shipping_Company bo = new Shipping_Company();
                    bo.ID = Convert.ToInt16(dr["ID"]);
                    bo.Company_name = dr["Company_name"].ToString();

                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return list_result;
        }
        public static List<Patients> list_Patients(string SiteID, string formname)
        {
            List<Patients> list_result = new List<Patients>();
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry.Append("SELECT id, Patient_ID FROM[T9_Patients] where Record_Status = 'Active' and ScreenFailure = 0 and Institution = '" + SiteID + "'");
                if (formname == "BLK")
                    sb_qry.Append("and FormBLKCreatedDate is null");
                if (formname == "FT")
                    sb_qry.Append(" and FormFTCreatedDate is null");


                SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                sb_qry.Clear();
                while (dr.Read())
                {
                    Patients bo = new Patients();
                    bo.ID = Convert.ToInt16(dr["ID"]);
                    bo.Patient_ID = dr["Patient_ID"].ToString();
                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return list_result;
        }
        public static List<Receieved_Conditions> list_ReceivedCondition()
        {
            List<Receieved_Conditions> list_result = new List<Receieved_Conditions>();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("SELECT [ReceievedCondition_ID],[ReceievedCondition_Name] FROM [T51_Receieved_Conditions] where Record_Status='Active'", Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Receieved_Conditions bo = new Receieved_Conditions();
                    bo.ReceievedCondition_ID = Convert.ToInt16(dr["ReceievedCondition_ID"]);
                    bo.ReceievedCondition_Name = dr["ReceievedCondition_Name"].ToString();
                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return list_result;
        }

        public static List<Protocol_BO> List_Protocols(int User_ID)
        {
            List<Protocol_BO> list_result = new List<Protocol_BO>();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("select t2.id,t2.Protocol_Code from T1_User_Main t1 join T2_Protocol_Main t2  on (t2.Id=t1.Protocol_Id and t2.Record_Status='Active') where t1.User_ID=" + User_ID + " and t1.Record_Status='Active'", Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Protocol_BO bo = new Protocol_BO();
                    bo.Protocol_Id = Convert.ToInt16(dr["id"]);
                    bo.Protocol_Code = dr["Protocol_Code"].ToString();
                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return list_result;
        }
        public static List<Forms> Listforms(int ProtocolID)
        {
            List<Forms> list_result = new List<Forms>();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("select t100.T7_FormID,t7.Form_Name,t7.URL from T7_Forms t7 join[T100_Map_T2Protocol_T7Forms] t100 on(t100.T7_FormID = t7.Form_ID and t100.Record_Status = 'Active') where t100.T2_ProtocolID = " + ProtocolID, Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Forms bo = new Forms();
                    bo.ID = Convert.ToInt16(dr["T7_FormID"]);
                    bo.Form_Name = dr["Form_Name"].ToString();
                    bo.URL = dr["URL"].ToString();
                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return list_result;
        }

    }

}

