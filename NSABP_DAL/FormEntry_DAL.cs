﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSABP_Model_BO_;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace NSABP_DAL
{

    public class FormEntry_DAL
    {
        private static string Co = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
        private static SqlConnection Conn;
        private static StringBuilder sb_qry;
        public static bool SaveFormEntryDetails(FormEntry_BO objBO)
        {

            bool isSave = false;
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                sb_qry.Append(" insert into [T9_Patients]([Patient_ID] ,   [EntryDate]   ,   [Institution]   ,     [ScreenFailure]  ,      [Patient_Status], [Created_by]   , [Created_on]   ,  [Modified_By]  ,    [Modified_On] ,    [Record_Status],Consent_Date_Shipped,Protocol_Id ) ");
                sb_qry.Append(" values('" + objBO.PatientID + "',");
                if (objBO.EntryDate != null)
                    sb_qry.Append("    '" + objBO.EntryDate + "',");
                else
                    sb_qry.Append("NULL,");
                sb_qry.Append(" '" + objBO.SiteID + "','" + objBO.ScreenFailure + "',  ");
                sb_qry.Append(" '" + objBO.Patient_Status + "',");
                sb_qry.Append(" '" + objBO.Created_by + "',GETDATE(),");
                sb_qry.Append(" '" + objBO.Modified_By + "',GETDATE(),'Active',");
                if (objBO.Consent_Date_Shipped != null)
                    sb_qry.Append("   '" + objBO.Consent_Date_Shipped + "',"+objBO.protocolID+")");
                else
                sb_qry.Append("NULL,"+ objBO.protocolID + ")");


                SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                cmd.ExecuteNonQuery();
                Conn.Close();
                SqlCommand cmd1 = new SqlCommand("[Generate_LabNumber_Proc]", Conn);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@PatientID", objBO.PatientID);
                cmd1.Parameters.AddWithValue("@Protocol", objBO.PatientID_Part1);//Protocol
                Conn.Open();
                cmd1.ExecuteNonQuery();
                Conn.Close();
                isSave = true;
            }
            catch (Exception ex)
            {

            }
            return isSave;
        }
        public static bool UpdateFormEntry(FormEntry_BO objBO)
        {
            bool isUpdate = false;
            try
            {
                //SqlConnection con = new SqlConnection("co");
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                sb_qry.Append("update  [T9_Patients] set [EntryDate]='" + objBO.EntryDate + "' ,[Patient_Status]='" + objBO.Patient_Status + "',ScreenFailure='" + objBO.ScreenFailure + "',[Consent_Date_Shipped]='" + objBO.Consent_Date_Shipped + "' ,[Modified_On]=GETDATE(),[Modified_By]=1 where id=" + objBO.ID);

                SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                cmd.ExecuteNonQuery();
                Conn.Close();
                isUpdate = true;
            }
            catch (Exception ex)
            {
            }
            return isUpdate;
        }
        public static List<FormEntry_BO> GetFormEntry_Details(int protocolID)
        {

            List<FormEntry_BO> List_FormEntry_BO = new List<FormEntry_BO>();
            sb_qry = new StringBuilder();
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry.Append(" select   t9. [id]  ,  t9. [Patient_ID] , t9.[Lab_Number], t9.[EntryDate],t8.SiteID,t8.Site_Name, t9.[FormBLKCreatedDate], t9.[FormBLKDITConfirmedDate] ,  ");
                sb_qry.Append("   t9.[FormBLKLabReceiveddDate] , t9.[ScreenFailure]  , t9.[Deceased]   , t9.[Patient_Status]  , t9.[Storage_Seq_Num] , t9.[Created_by]   , ");
                sb_qry.Append("    t9.[Created_on] , t9.[Modified_By]    , t9.[Modified_On]     , t9.[Record_Status]  , t9.[Consent_Date_Shipped],	    t9. [FormBNK1CreatedDate],    t9. [FormBNK1LabReceiveddDate],");
                sb_qry.Append("  t9.[FormBNK2CreatedDate] ,     t9.[FormBNK2LabReceiveddDate],     t9.[FormBNK3CreatedDate] ,  ");
                sb_qry.Append("  t9.[FormBNK3LabReceiveddDate], t9.[FormFTCreatedDate] ,     t9.[ReturnForm] FROM [T9_Patients] t9 LEFT JOIN T1_User_Main t1 ON (T1.User_ID=T9.Created_by) left join T8_Sites t8 on (t9.Institution=t8.SiteID and t8.Record_Status='Active') ");
                sb_qry.Append("     where t9.Protocol_Id=" + protocolID + "");



       SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    FormEntry_BO bo = new FormEntry_BO();
                    bo.ID = Convert.ToInt32(dr["ID"].ToString());
                    bo.PatientID = dr["Patient_ID"].ToString();
                    // bo.Site_Name = dr["Site_Name"].ToString();
                    //bo.SiteID = dr["InstitutionNumber"].ToString();
                    bo.Lab_Number = dr["Lab_Number"].ToString();
                    if (dr["EntryDate"] != DBNull.Value)
                        bo.EntryDate = Convert.ToDateTime(dr["EntryDate"].ToString());
                    bo.SiteID = dr["SiteID"].ToString();
                    bo.Site_Name = dr["Site_Name"].ToString();
                    if (dr["FormBLKCreatedDate"] != DBNull.Value)
                        bo.FormBLKCreatedDate = Convert.ToDateTime(dr["FormBLKCreatedDate"].ToString());
                    if (dr["FormBLKDITConfirmedDate"] != DBNull.Value)
                        bo.FormBLKDITConfirmedDate = Convert.ToDateTime(dr["FormBLKDITConfirmedDate"].ToString());
                    if (dr["FormBLKLabReceiveddDate"] != DBNull.Value)
                        bo.FormBLKLabReceiveddDate = Convert.ToDateTime(dr["FormBLKLabReceiveddDate"].ToString());
                    if (dr["ScreenFailure"] != DBNull.Value)
                        bo.ScreenFailure = (bool)dr["ScreenFailure"];
                    if (dr["Deceased"] != DBNull.Value)
                        bo.Deceased = (bool)dr["Deceased"];
                    if (dr["Patient_Status"] != DBNull.Value)
                        bo.Patient_Status = dr["Patient_Status"].ToString();
                    bo.Storage_Seq_Num = dr["Storage_Seq_Num"].ToString();
                    if (dr["Created_by"] != DBNull.Value)
                        bo.Created_by = Convert.ToInt32(dr["Created_by"].ToString());
                    //bo.Created_on = Convert.ToDateTime(dr["Created_on"].ToString());
                    //bo.Modified_By = Convert.ToInt32(dr["Modified_By"].ToString());
                    //bo.Modified_On = Convert.ToDateTime(dr["Modified_On"].ToString());
                    bo.Record_Status = dr["Record_Status"].ToString();
                    if (dr["Consent_Date_Shipped"] != DBNull.Value)
                        bo.Consent_Date_Shipped = Convert.ToDateTime(dr["Consent_Date_Shipped"].ToString());
                    if (dr["FormBNK1CreatedDate"] != DBNull.Value)
                        bo.FormBNK1CreatedDate = Convert.ToDateTime(dr["FormBNK1CreatedDate"].ToString());
                    if (dr["FormBNK1LabReceiveddDate"] != DBNull.Value)
                        bo.FormBNK1LabReceiveddDate = Convert.ToDateTime(dr["FormBNK1LabReceiveddDate"].ToString());
                    if (dr["FormBNK2CreatedDate"] != DBNull.Value)
                        bo.FormBNK2CreatedDate = Convert.ToDateTime(dr["FormBNK2CreatedDate"].ToString());
                    if (dr["FormBNK2LabReceiveddDate"] != DBNull.Value)
                        bo.FormBNK2LabReceiveddDate = Convert.ToDateTime(dr["FormBNK2LabReceiveddDate"].ToString());
                    if (dr["FormBNK3CreatedDate"] != DBNull.Value)
                        bo.FormBNK3CreatedDate = Convert.ToDateTime(dr["FormBNK3CreatedDate"].ToString());
                    if (dr["FormBNK3LabReceiveddDate"] != DBNull.Value)
                        bo.FormBNK3LabReceiveddDate = Convert.ToDateTime(dr["FormBNK3LabReceiveddDate"].ToString());
                    bo.Form_Return = dr["ReturnForm"].ToString();
                    if (dr["FormBNK3LabReceiveddDate"] != DBNull.Value)
                        bo.FormBNK3LabReceiveddDate = Convert.ToDateTime(dr["FormBNK3LabReceiveddDate"].ToString());
                    if(dr["FormFTCreatedDate"]!=DBNull.Value)
                    {
                        bo.FormFTCreatedDate= Convert.ToDateTime(dr["FormFTCreatedDate"].ToString());
                    }

                    List_FormEntry_BO.Add(bo);
                }
                Conn.Close();

            }
            catch (Exception ex)
            {

            }

            return List_FormEntry_BO;
        }
        public static FormEntry_BO GetFormEntry_Details_ByID(int ID)
        {


            sb_qry = new StringBuilder();
            FormEntry_BO bo = new FormEntry_BO();
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry.Append(" select   t9. [id]  ,  t9. [Patient_ID] , t9.[Lab_Number], CAST( t9.[EntryDate] as date) as [EntryDate],t8.SiteID,t8.Site_Name, t9.[FormBLKCreatedDate], t9.[FormBLKDITConfirmedDate] ,  ");
                sb_qry.Append("   t9.[FormBLKLabReceiveddDate] , t9.[ScreenFailure]  , t9.[Deceased]   , t9.[Patient_Status]  , t9.[Storage_Seq_Num] , t9.[Created_by]   , ");
                sb_qry.Append("    t9.[Created_on] , t9.[Modified_By]    , t9.[Modified_On]     , t9.[Record_Status]  , CAST(t9.[Consent_Date_Shipped] as date) as [Consent_Date_Shipped],	    t9. [FormBNK1CreatedDate],    t9. [FormBNK1LabReceiveddDate],");
                sb_qry.Append("  t9.[FormBNK2CreatedDate] ,     t9.[FormBNK2LabReceiveddDate],     t9.[FormBNK3CreatedDate] ,  ");
                sb_qry.Append("  t9.[FormBNK3LabReceiveddDate],t9.[FormFTCreatedDate] ,t9.[ReturnForm] FROM [T9_Patients] t9 LEFT JOIN T1_User_Main t1 ON (T1.User_ID=T9.Created_by) left join T8_Sites t8 on (t9.Institution=t8.SiteID and t8.Record_Status='Active')  where t9. [id]=" + ID + " and  t9.Record_Status='Active'");
                SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    //FormEntry_BO bo = new FormEntry_BO();
                    bo.ID = Convert.ToInt32(dr["ID"].ToString());
                    bo.PatientID = dr["Patient_ID"].ToString();
                    // bo.Site_Name = dr["Site_Name"].ToString();
                    //bo.SiteID = dr["InstitutionNumber"].ToString();
                    bo.Lab_Number = dr["Lab_Number"].ToString();
                    if (dr["EntryDate"] != DBNull.Value)
                        bo.EntryDate = Convert.ToDateTime(dr["EntryDate"].ToString());
                    bo.SiteID = dr["SiteID"].ToString();
                    bo.Site_Name = dr["Site_Name"].ToString();
                    if (dr["FormBLKCreatedDate"] != DBNull.Value)
                        bo.FormBLKCreatedDate = Convert.ToDateTime(dr["FormBLKCreatedDate"].ToString());
                    if (dr["FormBLKDITConfirmedDate"] != DBNull.Value)
                        bo.FormBLKDITConfirmedDate = Convert.ToDateTime(dr["FormBLKDITConfirmedDate"].ToString());
                    if (dr["FormBLKLabReceiveddDate"] != DBNull.Value)
                        bo.FormBLKLabReceiveddDate = Convert.ToDateTime(dr["FormBLKLabReceiveddDate"].ToString());
                    if (dr["ScreenFailure"] != DBNull.Value)
                        bo.ScreenFailure = (bool)dr["ScreenFailure"];
                    if (dr["Deceased"] != DBNull.Value)
                        bo.Deceased = (bool)dr["Deceased"];
                    if (dr["Patient_Status"] != DBNull.Value)
                        bo.Patient_Status = dr["Patient_Status"].ToString();
                    bo.Storage_Seq_Num = dr["Storage_Seq_Num"].ToString();
                    if (dr["Created_by"] != DBNull.Value)
                        bo.Created_by = Convert.ToInt32(dr["Created_by"].ToString());
                    //bo.Created_on = Convert.ToDateTime(dr["Created_on"].ToString());
                    //bo.Modified_By = Convert.ToInt32(dr["Modified_By"].ToString());
                    //bo.Modified_On = Convert.ToDateTime(dr["Modified_On"].ToString());
                    bo.Record_Status = dr["Record_Status"].ToString();
                    if (dr["Consent_Date_Shipped"] != DBNull.Value)
                        bo.Consent_Date_Shipped = Convert.ToDateTime(dr["Consent_Date_Shipped"].ToString());
                    if (dr["FormBNK1CreatedDate"] != DBNull.Value)
                        bo.FormBNK1CreatedDate = Convert.ToDateTime(dr["FormBNK1CreatedDate"].ToString());
                    if (dr["FormBNK1LabReceiveddDate"] != DBNull.Value)
                        bo.FormBNK1LabReceiveddDate = Convert.ToDateTime(dr["FormBNK1LabReceiveddDate"].ToString());
                    if (dr["FormBNK2CreatedDate"] != DBNull.Value)
                        bo.FormBNK2CreatedDate = Convert.ToDateTime(dr["FormBNK2CreatedDate"].ToString());
                    if (dr["FormBNK2LabReceiveddDate"] != DBNull.Value)
                        bo.FormBNK2LabReceiveddDate = Convert.ToDateTime(dr["FormBNK2LabReceiveddDate"].ToString());
                    if (dr["FormBNK3CreatedDate"] != DBNull.Value)
                        bo.FormBNK3CreatedDate = Convert.ToDateTime(dr["FormBNK3CreatedDate"].ToString());
                    if (dr["FormBNK3LabReceiveddDate"] != DBNull.Value)
                        bo.FormBNK3LabReceiveddDate = Convert.ToDateTime(dr["FormBNK3LabReceiveddDate"].ToString());
                    bo.Form_Return = dr["ReturnForm"].ToString();
                    if (dr["FormBNK3LabReceiveddDate"] != DBNull.Value)
                        bo.FormBNK3LabReceiveddDate = Convert.ToDateTime(dr["FormBNK3LabReceiveddDate"].ToString());



                }
                Conn.Close();

            }
            catch (Exception ex)
            {
            }

            return bo;
        }
        public static bool IsPatient_IDExists(string Patient_ID)
        {
            bool isExists = false;
            try
            {
                //SqlConnection con = new SqlConnection("co");
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                sb_qry.Append("select Count(*) from T9_Patients where Patient_ID='" + Patient_ID + "' and Record_Status='Active'");

                SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                Conn.Close();
                if (count != 0)
                    isExists = true;
            }
            catch (Exception ex)
            {
            }
            return isExists;
        }
    }
}
