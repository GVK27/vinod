﻿using System.Web;
using System.Web.Optimization;

namespace NSABP_POC
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/assets/js").Include(
                        "~/assets/node_modules/jquery/jquery-3.2.1.min.js",
    "~/assets/node_modules/popper/popper.min.js",
    "~/assets/node_modules/bootstrap/dist/js/bootstrap.min.js",
    //!--slimscrollbar scrollbar JavaScript-- >
    "~/dist/js/perfect-scrollbar.jquery.min.js",
    //!--Wave Effects-- >
    "~/dist/js/waves.js",
    //!--Menu sidebar-- >
    "~/dist/js/sidebarmenu.js",
    //!--stickey kit-- >
    "~/assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js",
    "~/assets/node_modules/sparkline/jquery.sparkline.min.js",
    //!--Custom JavaScript-- >
    "~/dist/js/custom.min.js",
     "~/assets/node_modules/moment/moment.js",
    "~/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js",
    "~/assets/node_modules/datatables/datatables.min.js",
      //"~/assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js",
    "~/assets/node_modules/timepicker/bootstrap-timepicker.min.js",
    "~/assets/node_modules/bootstrap-daterangepicker/daterangepicker.js"
  


     ));
            bundles.Add(new StyleBundle("~/Content/css").Include(
    "~/dist/css/style.min.css",
    "~/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css",
     "~/assets/node_modules/datatables/media/css/dataTables.bootstrap4.css",
     "~/assets/node_modules/datatables/datatables.min.css", 
     "~/assets/node_modules/timepicker/bootstrap-timepicker.min.css"
     , "~/assets/node_modules/bootstrap-daterangepicker/daterangepicker.css"

    ));
        }
    }
}



