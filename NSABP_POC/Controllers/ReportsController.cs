﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using NSABP_BLL;
using NSABP_Model_BO_;

namespace NSABP_POC.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports
        public ActionResult Reports()
        {
            return View();
        }

        public ActionResult GetReports(string startdate, string enddate)
        {

            DataTable dt = new DataTable();
            DateTime _startdate = startdate != "" ? Convert.ToDateTime(startdate) : DateTime.MinValue;
            DateTime _enddate = enddate != "" ? Convert.ToDateTime(enddate) : DateTime.MinValue;
             string site="", Role; int ProtocolId;
            Role = Session["Role_Name"].ToString();
            if (Role == "SiteUser")
                site = Session["Site"].ToString();
            ProtocolId = Convert.ToInt16(Session["Protocol_Id"]);
            dt = Reports_BLL.Reports(_startdate.Date, _enddate.Date, ProtocolId, Role, site);

            List<Report_BO> Report_BO = new List<Report_BO>();
            Report_BO = (from DataRow dr in dt.Rows
                         select new Report_BO()
                         {
                             ID = Convert.ToInt32(dr["ID"]),
                             FormID = dr["Form ID"].ToString(),
                             SiteID = dr["Site ID"].ToString(),
                             SiteName = dr["Site Name"].ToString(),
                             CreatedBy = dr["Created By"].ToString(),
                             CreatedOn = dr["Created On"].ToString(),
                             LabNumber = dr["Lab Number"].ToString(),
                             PatientID = dr["Patient ID"].ToString(),
                             ReceievedBy = dr["Receieved By"].ToString(),
                             ReceivedOn = dr["Received On"].ToString(),
                             ShippingCompany = dr["Shipping Company"].ToString(),
                             StorageNo = dr["Storage No"].ToString(),
                             TrackingNumber = dr["Tracking Number"].ToString(),

                         }).ToList();

            return Json(new { data = Report_BO }, JsonRequestBehavior.AllowGet);
        }
    }
}