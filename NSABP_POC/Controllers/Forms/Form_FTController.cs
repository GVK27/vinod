﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NSABP_Model_BO_;
using NSABP_BLL;
using System.Data;

namespace NSABP.Controllers.Forms.FormFT
{
    public class Form_FTController : Controller
    {
        // GET: Form_FT
        [HttpGet]
        public ActionResult Add_FormFT()
        {
            FormFT_BO bomodel = new FormFT_BO();
            try
            {
                bomodel.listShipping = Base_BLL.list_Shipping_Company();
                //bomodel.listPatients = Base_BLL.list_Patients();
                ViewBag.Patients = new SelectList("", "Patient_ID", "Patient_ID");
                bomodel.listSites = Base_BLL.list_Sites().Where(e => e.SiteID.StartsWith(Session["Site"].ToString().Split('-')[0])).ToList();
                bomodel.FormFT_PersonCompletingForm_Phone = Session["PhoneNumber"].ToString();
                bomodel.FormFT_PersonCompletingForm_Email = Session["email"].ToString();
                bomodel.FormFT_PersonCompletingForm_FName = Session["FName"].ToString();
                bomodel.FormFT_PersonCompletingForm_LName = Session["LName"].ToString();
                ViewBag.SiteOfOtherTumorTissues = new SelectList(FormFT_BLL.SiteOfOtherTumorTissues().AsDataView(), "Id", "Name");
            }
            catch (Exception ex)
            {

            }

            return View(bomodel);
        }
        [HttpPost]
        public JsonResult Add_FormFT(FormFT_BO bo)//, string save, string ship
        {
            try
            {
                bo.listSites = Base_BLL.list_Sites();
                bo.Created_By = Session["User_ID"].ToString();
                bo.Modified_By = Session["User_ID"].ToString();
                bo.Form_FT_ID = bo.T9_PatientID + "-FT";
                if (FormFT_BLL.SaveFormFTDetails(bo))
                {
                    return Json(new { status = "Success" });
                }

            }
            catch (Exception ex)
            {
            }
            return Json(new { status = "invalid" }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult Shipping(FormFT_BO bo)
        {
            try
            {
                bo.Form_FT_ID = bo.T9_PatientID + "-FT";
                if (FormFT_BLL.SaveShipmentDetails(bo))
                {
                    return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(new { status = "invalid" }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetFormFT_Details()
        {
            Protocol_role_site bo = new Protocol_role_site();
            bo.Role_Name = Session["Role_Name"].ToString();
            bo.ProtocolID = Convert.ToInt32(Session["Protocol_Id"]);

            if (Session["Site"] != "")
            {
                bo.T8_sites_Id = Session["Site"].ToString();
            }

            //List<FormFT_BO> FormFTDetails = FormFT_BLL.getFormFTDetailsByUserID(Session["Site"].ToString(), Session["Role_Name"].ToString());
            List<FormFT_BO> FormFTDetails = FormFT_BLL.getFormFTDetailsByUserID(bo);
            return View(FormFTDetails);
        }
        [HttpGet]
        public ActionResult EditFormFT(int ID)
        {

            FormFT_BO FormFTDetails = FormFT_BLL.getFormFtDetailsbyFormFtid(ID).FirstOrDefault();
            FormFTDetails.listShipping = Base_BLL.list_Shipping_Company();
            List<SelectListItem> Patients = new List<SelectListItem>();
            Patients.Add(new SelectListItem() { Text = FormFTDetails.T9_PatientID, Value = FormFTDetails.T9_PatientID });
            ViewBag.Patients = new SelectList(Patients, "Value", "Text");

            FormFTDetails.listSites = Base_BLL.list_Sites();
            ViewBag.SiteOfOtherTumorTissues = new SelectList(FormFT_BLL.SiteOfOtherTumorTissues().AsDataView(), "Id", "Name");
            return PartialView("P_FormFT_Add", FormFTDetails);
        }
        [HttpPost]
        public ActionResult UpdateFormFTDetails(FormFT_BO bo)
        {
            try
            {
                bo.Modified_By = Session["User_ID"].ToString();
                if (FormFT_BLL.UpdateFormFt(bo))
                {
                    return Json(new { status = "FormFT Details updated successfully " });
                }

            }
            catch (Exception ex)
            {
            }

            return Json(new { status = "Invalid operation" });

        }
        [HttpGet]
        public ActionResult GetFormFTDetailsforDMG()
        {
            List<FormFT_BO> FormFTDetails = FormFT_BLL.getFormFTDetailsByUserID("", "DMG");
            return View(FormFTDetails);
        }
        /// <summary>
        /// code for open the dmg recieved condtions partial view with data
        /// </summary>
        /// <param name="id">FormFt table Auto id</param>
        /// <returns>partialview of dmg recieved</returns>
        public ActionResult GetReceivedConditions(int id)
        {
            FormFT_BO FormFTDetails = FormFT_BLL.GetReceivedDetailsforDMG(id);
            try
            {
                // FormFT_BO FormFTDetails = new FormFT_BO();
                ViewBag.ReceivedConditoins = new SelectList(FormFT_BLL.GetReceievedConditions().AsDataView(), "ReceievedCondition_ID", "ReceievedCondition_Name");
                ViewBag.SlidesLocations = new SelectList(FormFT_BLL.GetSlides_Locations().AsDataView(), "Location_ID", "Location_Name");
                ViewBag.Slides_Status = new SelectList(FormFT_BLL.GetSlides_Status().AsDataView(), "Status_ID", "Status_Name");

            }
            catch (Exception ex)
            {

            }

            return PartialView("~/Views/Form_FT/P_DMGReceive.cshtml", FormFTDetails);

        }
        [HttpPost]
        public JsonResult SaverecievedCondtions(FormFT_BO bo)
        {//code for dmg recieve
            try
            {
                bo.DMG_Receieved_By = Session["User_ID"].ToString();
                if (FormFT_BLL.saveReceivedDetails(bo))
                {
                    return Json(new { status = "Success" });
                }

            }
            catch (Exception ex)
            {

            }
            return Json(new { status = "Invalid operation" });
        }

        public ActionResult clear()
        {
            ModelState.Clear();
            return View(" Add_FormFT");
        }
        public JsonResult getSiteName(string id)
        {
            try
            {
                FormFT_BO bomodel = new FormFT_BO();
                bomodel.listSites = Base_BLL.list_Sites();
                var sitename = bomodel.listSites.Where(x => x.SiteID == id).Select(x => x.Site_Name).FirstOrDefault();
                ViewBag.Patients = new SelectList(Base_BLL.list_Patients(id, "FT").Where(x => x.Patient_ID.Contains(Session["Protocol_Code"].ToString())), "Patient_ID", "Patient_ID");
                return Json(new { Patients = ViewBag.Patients, sitename = sitename });
            }
            catch (Exception ex)
            {
            }

            return Json(new { invalid = "" });
        }
    }
}