﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSABP_DAL;
using System.Data;
using NSABP_Model_BO_;

namespace NSABP_BLL
{
    public class Configuration_BLL
    {
        public static List<checkboxes_BO> GetForms()
        {
            List<checkboxes_BO> list_forms = new List<checkboxes_BO>();
            try
            {
                list_forms = Configuration_DAL.GetForms();
            }
            catch (Exception ex)
            {
            }
            return list_forms;
        }
        public static Boolean Config_Protocol_form(int Protocol_ID, string[] Form_ID)
        {
            bool _isSave = false;
            try
            {
                _isSave = Configuration_DAL.Config_Protocol_form(Protocol_ID, Form_ID);
            }
            catch (Exception ex)
            {
            }
            return _isSave;

        }
        public static List<int> GetForms_IDS(int protocol_ID)
        {
            List<int> res = new List<int>();
            try
            {
                res = Configuration_DAL.GetForms_IDS(protocol_ID);
            }
            catch (Exception ex)
            {
            }
            return res;
        }

        //changes start For Timepoint10/10
        public static bool Config_Protocol_form_Timepoint(List<checkboxes_BO> listbo)
        {
            bool _isSave = false;
            try
            {
                _isSave = Configuration_DAL.Config_Protocol_form_Timepoint(listbo);
            }
            catch (Exception ex)
            {
            }
            return _isSave;
        }

        public static DataTable GetForms_TimePoints(int protocol_ID)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = Configuration_DAL.GetForms_TimePoints(protocol_ID);

            }
            catch (Exception ex)
            {
            }
            return dt;
        }
        //changes end For Timepoint10/10
    }
}
