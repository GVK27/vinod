﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace NSABP_Model_BO_
{
    public class FormFT_BO
    {
        public int ID { get; set; }
        public string Form_FT_ID { get; set; }
        [DisplayName("Patient ID:")]
        public string T9_PatientID { get; set; }
        public string User_Main_Id { get; set; }
        [DisplayName("Liver Tumor Tissue")]
        public bool LiverTumorIssue { get; set; }
        [DisplayName("Other Tumor Tissue")]
        public bool OtherTIssue { get; set; }
        public int? SiteOfOtherTumorTIssue { get; set; }
        public string SiteOfOtherTumorTIssueName { get; set; }        
        [DisplayName("Date tumor biopsy specimens collected")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateBiospecimenCollected { get; set; }
        [DisplayName(" Core Biopsy specimen submerged in 10% Formaline Vial Collected?")]
        public string SubMergedIn10PercentFormalinVial { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public string Modified_By { get; set; }
        public DateTime Modified_On { get; set; }

        public int? shippingCompanyId { get; set; }
        [DisplayName("Name of the Shipping Company:")]
        public string shippingCompanyName { get; set; }
        [DisplayName(" Tracking Number: ")]
        public string trackingNumber { get; set; }
     
        [DisplayName("Date Specimens shipped to Division of Pathology :")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dateShippedToDivisionOfPathology { get; set; }
        public string DMG_Receieved_By { get; set; }
        public DateTime? DMG_Receieved_Date { get; set; }
        public int Received_Condition { get; set; }
        public string Record_Status { get; set; }
        [DisplayName("Institution Number:  ")]
        public string SiteID { get; set; }
        [DisplayName("Institution Name:")]
        public string Site_Name { get; set; }
        public string FormFT_PersonCompletingForm_LName { get; set; }
        public string FormFT_PersonCompletingForm_FName { get; set; }
        public string FormFT_PersonCompletingForm_Phone { get; set; }
        public string FormFT_PersonCompletingForm_Email { get; set; }
        public string Lab_Number { get; set; }
        public string StorageSequenceNo { get; set; }
        public User_BO user { get; set; }
        public List<Sites> listSites { get; set; }
        public List<Patients> listPatients { get; set; }
        public List<Shipping_Company> listShipping { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? RecieptDate { get; set; }
        public int SlidesLocationID { get; set; }
        public int Slides_Status_ID { get; set; }
        public string ReceivedNote { get; set; }
        public string CoreId { get; set; }







    }
}
