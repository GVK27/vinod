﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace NSABP_Model_BO_
{
    public class FormBLK_BO
    {
        public string temp { get; set; }
        public List<Sites> List_Sites { get; set; }
        public List<FormBLK_Blocks_BO> List_Blocks { get; set; }
        public List<Patients> list_Patients { get; set; }
        public List<Shipping_Company> list_Shipping_Company { get; set; }
        public List<Receieved_Conditions> list_Receieved_Conditions { get; set; }

        public string Slide_ID { get; set; }
        public int ID { get; set; }
        public string Form_BlockID { get; set; }
        [Required]
        [Display(Name = "Patient ID")]
        public string T1_PatientID { get; set; }


        [Display(Name = "Institution Name")]
        public string Site_Name { get; set; }
        [Required]
        [Display(Name = "Institution Number")]
        public string SiteID { get; set; }
        [Display(Name = "Last Name")]
        public string FormBLK_PersonCompletingForm_LName { get; set; }
        [Display(Name = "First Name")]
        public string FormBLK_PersonCompletingForm_FName { get; set; }
        [Display(Name = "Phone number")]
        public string FormBLK_PersonCompletingForm_Phone { get; set; }
        [Display(Name = "Email")]
        public string FormBLK_PersonCompletingForm_Email { get; set; }
        [Display(Name = "Pathology Accession Number:")]
        public string FormBLK_Patology_Accession_Number { get; set; }
        [Display(Name = "Is this the sample from Diagnostic Breast Biopsy?")]
        public bool? isFromDiagnosticBreastBiopsy { get; set; }
        [Display(Name = "FFPE from the diagnostic breast biopsy specimen")]
        public bool FPPE_FromTheDiagnostiBreastBiopsySpecimen { get; set; }
        [Display(Name = "Unstained slides from the diagnostic breast biopsy specimen")]

        public bool UnstainedSlidesFromDiagnosticBreastBiopsy { get; set; }
        [Display(Name = "NSABP Pathology Accession Number:")]
        public string NSABP_Patology_Accession_Number { get; set; }
        public int numberOfSlides { get; set; }
        public int numberOfBlocks { get; set; }
        public int DMG_Receieved_By { get; set; }
        public string DMG_Receieved_By_Name { get; set; }
        public string Notes { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DMG_Receieved_Date { get; set; }


        public string Company_name { get; set; }
        [Display(Name = "Name of the Shipping Company:")]
        public int shippingCompanyID { get; set; }
        [Display(Name = "Tracking Number:")]
        public string trackingNumber { get; set; }
        public string Created_By_Name { get; set; }
        public int Created_By { get; set; }
        [Display(Name = "Date Specimens shipped to Division of Pathology")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dateShippedToDivisionOfPathology { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Created_On { get; set; }
        public DateTime Modified_On { get; set; }
        public int Modified_By { get; set; }
        public DateTime DateDiagnosticBreastBiopsyProcedure { get; set; }
        public bool? FormEnclosed { get; set; }
        public int? numberOfSlidesReceivedByNSABP { get; set; }
        public int? numberOfBlocksReceivedByNSABP { get; set; }
        public bool? IsFormBlockReceived { get; set; }
        public bool? AllowSiteEdit { get; set; }
        public string Record_Status { get; set; }
        public string Receipt_Number { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? AllowSiteEditDate { get; set; }
        public String  Storage_Seq_Num { get; set; }
        public String Lab_Number { get; set; }

    }

    public class FormBLK_Slides_BO
    {
        public string Slide_ID { get; set; }
        public string Receieved_condition { get; set; }
        public int T21_PhaseII_Block_ID { get; set; }
        public int T50_Record_Created_By { get; set; }
        public string Record_Status { get; set; }
        public DateTime Created_On { get; set; }

    }

    public class FormBLK_Blocks_BO
    {
        //public string _blkids { get; set; }
        //public string _RcvdValue { get; set; }
        //public string _Reembd { get; set; }

        //public int _dmgrcvdblks { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dmgRcvddate { get; set; }
        public int ID { get; set; }
        public string Block_Slide_ID { get; set; }
        public string Receieved_condition { get; set; }
        public bool? Re_embed { get; set; }

        public int T2_PhaseII_FormBlk_ID { get; set; }
        public int T50_Record_Created_By { get; set; }
        public string Record_Status { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Created_On { get; set; }
        public int T60_Process { get; set; }
        public int T61_Status { get; set; }
        public int T62_Location { get; set; }
        public string Dmg_Block_ID { get; set; }


    }







}
