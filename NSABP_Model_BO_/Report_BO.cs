﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSABP_Model_BO_
{
    public class Report_BO
    {
        public int ID { get; set; }
        public string FormID { get; set; }
        public string SiteID { get; set; }
        public string SiteName { get; set; }
        public string PatientID { get; set; }
        public string LabNumber { get; set; }
        public string StorageNo { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string ShippingCompany { get; set; }
        public string TrackingNumber { get; set; }
        public string ReceievedBy { get; set; }
        public string ReceivedOn { get; set; }


    }
}
