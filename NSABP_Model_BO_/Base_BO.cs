﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace NSABP_Model_BO_
{
    public class Shipping_Company
    {
        public int ID { get; set; }
        public string Company_name { get; set; }
        public string Description { get; set; }
    }
    public class Sites
    {
        public int ID { get; set; }
        [Display(Name ="Institution Number")]
        public string SiteID { get; set; }
        [Display(Name = "Institution Name")]
        public string Site_Name { get; set; }
        public string PrimaryContact_Fname { get; set; }
        public string PrimaryContact_Lname { get; set; }
        public string PrimaryContact_Email { get; set; }
        public string PrimaryContact_Phone { get; set; }
        public int Created_by { get; set; }
        public int Modified_By { get; set; }
        public DateTime Created_on { get; set; }
        public DateTime Modified_On { get; set; }
        public string Record_Status { get; set; }
    }
    public class Receieved_Conditions
    {
        public int ReceievedCondition_ID { get; set; }
      
        public string ReceievedCondition_Name { get; set; }
        
    }
    public class Patients
    {
        public int ID { get; set; }
        [Display(Name = "Patient Name")]
        public string Patient_ID { get; set; }
        [Display(Name = "Lab Number Name")]
        public string Lab_Number { get; set; }
        public DateTime EntryDate { get; set; }
        public string Institution { get; set; }
        public DateTime FormBLKCreatedDate { get; set; }
        public DateTime FormBLKDITConfirmedDate { get; set; }
        public DateTime FormBLKLabReceiveddDate { get; set; }
        public bool ConsentWithdrawan { get; set; }
        public bool ReturnForm { get; set; }
        public DateTime Created_on { get; set; }
        public int Created_by { get; set; }
        public int Modified_By { get; set; }
        public DateTime Modified_On { get; set; }
        public string Record_Status { get; set; }

    }
 
    public class checkboxes_BO
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public bool _isChecked { get; set; }
        //changes10/10/18 for Timepoints
        public List<string> StrArray { get; set; }
        public int Protocol_ID { get; set; }
        //changes10/10/18 for Timepoints end
    }

    public class Protocol_BO
    {
        [Required]
        public int Protocol_Id { get; set; }
        [Display(Name = "Protocol")]
        public string Protocol_Code { get; set; }

    }
    public class Role_BO
    {
        [Required]
        public int Role_Id { get; set; }
        [Display(Name = "Role")]
        public string Role_Name { get; set; }
    }
    public class UserStatus_BO
    {
        [Required]
        public int Status_Id { get; set; }

        [Display(Name = "User Status")]
        public string Status_Title { get; set; }
    }
    public class Forms
    {
        public int ID { get; set; }
        public string Form_Name { get; set; }
        public string URL { get; set; }


    }
}
